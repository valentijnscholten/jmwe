package com.innovalog.jmwe.plugins.functions;

import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.googlecode.jsu.util.WorkflowUtils;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class CopyFieldValueToParentFunction extends AbstractPreserveChangesPostFunction {
  private Logger log = LoggerFactory.getLogger(CopyFieldValueToParentFunction.class);
  private static final String FIELD = "field";
  private final WorkflowUtils workflowUtils;
  private final IssueManager issueManager;

  public CopyFieldValueToParentFunction(WorkflowUtils workflowUtils, IssueManager issueManager) {
    this.workflowUtils = workflowUtils;
    this.issueManager = issueManager;
  }

  public void executeFunction(Map transientVars, Map args, PropertySet ps, IssueChangeHolder holder) throws WorkflowException {
    String fieldKey = (String) args.get(FIELD);
    Field field = (Field) workflowUtils.getFieldFromKey(fieldKey);
    if (field == null) {
      log.error("Error while executing function : field [" + fieldKey + "] not found");
      return;
    }

    try {
      MutableIssue issue = getIssue(transientVars);
      Object sourceValue = workflowUtils.getFieldValueFromIssue(issue, field, false);

      // get the parent issue
      MutableIssue parentIssue = (MutableIssue) issue.getParentObject();
      if (parentIssue != null) {
        workflowUtils.setFieldValue(parentIssue, field, sourceValue, new DefaultIssueChangeHolder());

        // trigger an edit on the issue
        issueManager.updateIssue(getCaller(transientVars, args), parentIssue, EventDispatchOption.ISSUE_UPDATED, false);
      }
    } catch (Exception e) {
      log.warn("Error while executing Copy Field Value to Parent function: " + e, e);
    }
  }
}
