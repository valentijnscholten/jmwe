package com.innovalog.jmwe.plugins.functions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.IssueManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.dispatcher.ActionResult;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.util.ImportUtils;
import com.googlecode.jsu.util.WorkflowUtils;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

public class AddFieldValueToParentFunction extends AbstractPreserveChangesPostFunction {
  private Logger log = LoggerFactory.getLogger(AddFieldValueToParentFunction.class);
  private static final String FIELD = "field";
  private final WorkflowUtils workflowUtils;
  private final IssueManager issueManager;

  public AddFieldValueToParentFunction(WorkflowUtils workflowUtils, IssueManager issueManager) {
    this.workflowUtils = workflowUtils;
    this.issueManager = issueManager;
  }

  public void executeFunction(Map transientVars, Map args, PropertySet ps, IssueChangeHolder holder)
    throws WorkflowException {
    String fieldKey = (String) args.get(FIELD);
    Field field = (Field) workflowUtils.getFieldFromKey(fieldKey);
    if (field == null) {
      log.warn("Error while executing function : field [" + fieldKey
        + "] not found");
      return;
    }

    try {
      MutableIssue issue = getIssue(transientVars);
      Object sourceValue = workflowUtils.getFieldValueFromIssue(issue,
        field, false);
      if (sourceValue != null && sourceValue instanceof Collection) {
        // get the parent issue
        MutableIssue parentIssue = (MutableIssue) issue.getParentObject();
        if (parentIssue != null) {
          //get parent issue's field value
          Object parentValue = workflowUtils.getFieldValueFromIssue(parentIssue, field, false);
          if (parentValue == null)
            parentValue = new ArrayList();
          if (parentValue instanceof Collection) {
            ((Collection) parentValue).addAll((Collection) sourceValue);
            workflowUtils.setFieldValue(parentIssue, field, parentValue, new DefaultIssueChangeHolder());

            // trigger an edit on the issue
            issueManager.updateIssue(getCaller(transientVars, args), parentIssue, EventDispatchOption.ISSUE_UPDATED, false);
          }
        }
      }
    } catch (Exception e) {
      log.warn("Error while executing function : " + e, e);
    }
  }
}
