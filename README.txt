The JIRA Misc Workflow Extensions plugin provides an assortment of workflow
conditions, validators and post-functions that you can use to implement
custom workflows in JIRA.


For more information, see: https://innovalog.atlassian.net/wiki/display/JMWE

For bug reports, see: https://innovalog.atlassian.net/browse/JMWE
